CheerMeOn::Application.routes.draw do
  resources :users, :goals
  resource :session, only: [:new, :create, :destroy]

end
