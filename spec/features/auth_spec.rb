require 'spec_helper'

describe "the signup process" do

  it "has a signup page" do
    visit new_user_url
    expect(page).to have_content "Sign Up"
  end

  describe "signing up a user" do
    before { visit new_user_url }

    it "redirects user to user show page on sign up" do
      fill_in 'Username:', :with => "test_user1"
      fill_in 'Password:', :with => "123456"
      click_button "Create User"
      expect(page).to have_content "test_user1"
    end

    describe "shows the signup page on signup fail" do
      it "fails on missing username" do
        fill_in 'Password:', :with => "123456"
        click_button "Create User"
        expect(page).to have_content "Sign Up"
      end

      it "fails on missing password" do
        fill_in "Username:", :with => "test_user1"
        click_button "Create User"
        expect(page).to have_content "Sign Up"
      end

      it "fails if username is taken" do
        fill_in "Username:", :with => "test_user1"
        fill_in "Password:", :with => "123456"
        click_button "Create User"

        click_button "Log Out"
        visit new_user_url

        fill_in "Username:", :with => "test_user1"
        fill_in "Password:", :with => "123456"
        click_button "Create User"

        expect(page).to have_content "Sign Up"
      end
    end

    describe "displays any errors on signup fail" do
      it "notifies user about mising password" do
        fill_in "Username:", :with => "test_user1"
        click_button "Create User"
        expect(page).to have_content "Password is too short"
      end

      it "notifies user about missing username" do
        fill_in 'Password:', :with => "123456"
        click_button "Create User"
        expect(page).to have_content "Username can't be blank"
      end

      it "notifies user about taken username" do
        fill_in "Username:", :with => "test_user1"
        fill_in "Password:", :with => "123456"
        click_button "Create User"

        click_button "Log Out"
        visit new_user_url

        fill_in "Username:", :with => "test_user1"
        fill_in "Password:", :with => "123456"
        click_button "Create User"

        expect(page).to have_content "Username has already been taken"
      end
    end
  end

end


describe "signing in" do
  it "has a sign in page" do
    visit new_session_url
    expect(page).to have_content "Sign In"
  end

  describe "signing in a user" do

    before do
      create_and_sign_in({:user_name => "user1", :password => "123456"})
      click_button "Log Out"
    end

    it "redirects user to user show page on sign in" do
      fill_in "Username:", :with => "user1"
      fill_in "Password:", :with => "123456"
      click_button "Sign In"

      expect(page).to have_content "user1"
    end

    describe "shows sign in page on fail" do

      it "fails on missing username" do
        fill_in "Password:", :with => "123456"
        click_button "Sign In"

        expect(page).to have_content "Sign In"
      end

      it "fails on missing password" do
        fill_in "Username:", :with => "user1"
        click_button "Sign In"

        expect(page).to have_content "Sign In"
      end

      it "fails when given incorrect username" do
        fill_in "Username:", :with => "I_dont_exist"
        click_button "Sign In"

        expect(page).to have_content "Sign In"
      end

      it "fails when given incorrect password" do
        fill_in "Password:", :with => "654321"
        click_button "Sign In"

        expect(page).to have_content "Sign In"
      end

    end

    describe "shows errors when given incorrect credentials" do


      it "notifies of error when given incorrect username" do
        fill_in "Password:", :with => "123456"
        click_button "Sign In"

        expect(page).to have_content "Invalid username or password"
      end

      it "notifies of error when given incorrect password" do
        fill_in "Username:", :with => "user1"
        click_button "Sign In"

        expect(page).to have_content "Invalid username or password"
      end

    end
  end

end

describe "log out" do


  before do
    create_and_sign_in({:user_name => "user1", :password => "123456"})
  end

  it "shows log out button if logged in" do
    expect(page).to have_button "Log Out"
  end

  it "does not show log out button if not logged in" do
    click_button "Log Out"
    expect(page).to_not have_button "Log Out"
  end

  it "should take user to sign in page on logout" do
    click_button "Log Out"
    expect(page).to have_content "Sign In"
  end


  it "does not show username after logout" do
    click_button "Log Out"
    expect(page).to_not have_content "user1"
  end

end