require 'spec_helper'

describe "goals functionality" do

  before do
    create_and_sign_in(user_name: 'user1', password: '123456')
    create_new_goal(title: "to finish Rspec testing", private: false)
    #assume redirect to goals_index
  end

  let (:user1) { User.first }
  let (:goal1) { Goal.first }

  it "requires log in to access new/edit/show goal pages" do
    click_button "Log Out"

    visit new_goal_url
    expect(page).to have_content("Sign In")

    visit edit_goal_url(1)
    expect(page).to have_content("Sign In")

    visit goal_url(1)
    expect(page).to have_content("Sign In")
  end

  describe "creating goals" do
    it "lets users create goals" do
      visit new_goal_url
      fill_in "Title:", :with => "climb mt everest"
      choose("Public")
      expect(page).to have_content "climb mt everest"

      visit new_goal_url
      fill_in "Title:", :with => "Beat Kyle at chess"
      choose("Private")
      expect(page).to have_content "Beat Kyle at chess"
    end
  end

  describe "shows users' goals" do
    it "shows all self goals" do
      create_new_goal(title: "make first goal", private: false)
      create_new_goal(title: "make second goal", private: true)

      visit user_url(user1)
      expect(page).to have_content("make first goal")
      expect(page).to have_content("make second goal")
    end

    it "shows only public goals on goals index" do
      create_new_goal(title: "#{user1.username} first goal", private: false)
      create_new_goal(title: "#{user1.username} second goal", private: true)

      click_button "Log Out"

      create_and_sign_in(user_name: "user2", password: "123456")
      create_new_goal(title: "user2 first goal", private: true)
      create_new_goal(title: "user2 second goal", private: false)

      visit goals_url
      expect(page).to have_content "#{user1.username} first goal"
      expect(page).to have_content "user2 second goal"
      expect(page).to_not have_content "#{user1.username} second goal"
      expect(page).to_not have_content "user2 first goal"
    end

    it "shows only public goals for on other users' show page" do
      create_new_goal(title: "#{user1.username} first goal", private: false)
      create_new_goal(title: "#{user1.username} second goal", private: true)

      click_button "Log Out"
      create_and_sign_in(user_name: "user2", password: "123456")

      visit user_url(user1)
      expect(page).to have_content "#{user1.username} first goal"
    end

  end

  describe "editing goals" do
    it "autofill goal attributes" do
      visit edit_goal_url(goal1)

      expect(page).to have_content "to finish Rspec testing"
      expect(page).to have_checked_field "Public"
    end

    it "redirects user to edited goal show page" do
      visit edit_goal_url(goal1)
      # original_title = goal1.title

      fill_in "Title:", :with => "Better goal"
      click_button "Update Goal"

      expect(page).to have_content "Better goal"
    end

    it "edited goal show page reflects changes" do
      visit edit_goal_url(goal1)
      original_title = goal1.title

      fill_in "Title:", :with => "Better goal"
      click_button "Update Goal"
      choose "Private"

      expect(page).to have_content "Better goal"
      expect(page).to have_content "Private"
      expect(page).to_not have_content original_title
      expect(page).to_not have_content "Public"
    end

    it "should only show edit option if goal belongs to user" do
      visit goal_url(goal1)
      expect(page).to have_button("Edit Goal")

      click_button "Logout"
      create_and_sign_in(user_name: "bob", password: "654321")

      visit goal_url(goal1)
      expect(page).to_not have_button("Edit Goal")
    end

    it "allows only goal's owner to edit goal" do
      visit goal_url(goal1)
      expect(page).to have_button("Update Goal")

      click_button "Logout"
      create_and_sign_in(user_name: "bob", password: "654321")

      visit goal_url(goal1)

      expect(page).to_not have_button("Update Goal")
      expect(page).to have_content("You cannot edit other users' goals")
    end


  end

  describe "deleting goals" do
    it "should only show delete option if goal belongs to user" do
      visit goal_url(goal1)
      expect(page).to have_button("Delete Goal")

      click_button "Logout"
      create_and_sign_in(user_name: "bob", password: "654321")

      visit goal_url(goal1)
      expect(page).to_not have_button("Delete Goal")
    end

    # it "allows only goal's owner to delete goal" do
    #   click_button "Logout"
    #   create_and_sign_in(user_name: "bob", password: "654321")
    #
    #   visit goal_url(goal1)
    #
    #
    # end



    it "should not show deleted goals" do
      visit goal_url(goal1)
      click_button "Delete Goal"

      visit user_url(user1)

      expect(page).to_not have_content(goal1.title)
      expect(page).to_not have_content("Goals")
    end

  end

end