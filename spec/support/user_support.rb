def create_and_sign_in(user_params)
  visit new_user_url
  fill_in "Username:", :with => user_params[:user_name]
  fill_in "Password:", :with => user_params[:password]
  click_button "Create User"
end