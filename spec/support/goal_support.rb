def create_new_goal(params)
  #params == {:title => "title", :privacy => "public/private"}

  visit new_goal_url

  privacy_option = (params[:private] == true) ? "Private" : "Public"

  fill_in "Title", with: params[:title]#"My great goal"
  choose(privacy_option)

  click_button "Create Goal"


end